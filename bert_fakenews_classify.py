"""
Experiments with fine-tuning BERT model for text classification
We try three truncation methods ways of dealing with long documents in the paper [1].
    head-only: keep the first 510 tokens;
    tail-only: keep the last 510 tokens;
    head+tail: empirically select the first 128 and the last 382 tokens.
"""
import os
import argparse
import random
import torch
from transformers import BertForSequenceClassification, BertTokenizer, BertConfig
from collections import defaultdict, Counter

from fakenews.datautils import load_fakenews_data, label2index
from fakenews.bertutils import prepare_data
from fakenews.trainer import train, evaluate


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--data_dir",
        required=True,
        type=str,
        help="Path to experimental data directory"
    )
    parser.add_argument(
        "--balanced",
        action="store_true", help="Whether to balanced the training data"
    )
    parser.add_argument(
        "--train_batch_size",
        type=int,
        default=8,
        help="Training batch size"
    )
    parser.add_argument(
        "--eval_batch_size",
        type=int,
        default=4,
        help="Evaluation batch size"
    )
    parser.add_argument(
        "--maxlen",
        type=int,
        default=256,
        help="Maximum sequence length"
    )
    parser.add_argument(
        "--gradient_accumulation_steps",
        type=int,
        default=4,
        help="Gradient accumulation steps"
    )
    parser.add_argument(
        "--seed",
        type=int,
        default=42,
        help="Random seed"
    )
    parser.add_argument(
        "--optim",
        type=str,
        default="AdamW",
        help="Optimization algorithm"
    )
    parser.add_argument(
        "--lr",
        type=float,
        default=2e-5,
        help="Learning rate (default is 2e-5)"
    )
    parser.add_argument(
        "--eps",
        type=float,
        default=1e-8,
        help="eps value for AdamW"
    )
    parser.add_argument(
        "--epochs",
        type=int,
        default=5,
        help="Training epochs"
    )
    args = parser.parse_args()
    args.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(args)

    train_texts, train_labels = load_fakenews_data(os.path.join(args.data_dir, "train.csv"))
    print(Counter(train_labels))
    if args.balanced:
        label2texts = defaultdict(list)
        for text, label in zip(train_texts, train_labels):
            label2texts[label].append(text)
        texts = label2texts[3.0]
        texts = random.sample(texts, 1000)
        label2texts[3.0] = texts
        
        train_texts, train_labels = [], []
        for label, texts in label2texts.items():
            for text in texts:
                train_texts.append(text)
                train_labels.append(label)
        print(Counter(train_labels))
        
    dev_texts, dev_labels = load_fakenews_data(os.path.join(args.data_dir, "dev.csv"))
    test_texts, test_labels = load_fakenews_data(os.path.join(args.data_dir, "test.csv"))
    
    train_y, dev_y, test_y, label2id, id2label = label2index(train_labels, dev_labels, test_labels)
    
    tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")
    
    train_dataset = prepare_data(train_texts, train_y, tokenizer)
    dev_dataset = prepare_data(dev_texts, dev_y, tokenizer)
    test_dataset = prepare_data(test_texts, test_y, tokenizer)
    
    config = BertConfig.from_pretrained('bert-base-uncased',
                                        num_labels=len(label2id))
    model = BertForSequenceClassification.from_pretrained("bert-base-uncased", config=config)
    model = model.to(args.device)
    
    train(args, train_dataset, dev_dataset, model)
    evaluate(args, model, test_dataset)
    
    
if __name__ == "__main__":
    main()

