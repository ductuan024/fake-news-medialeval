"""
Loading Fakenews data
"""
import pandas as pd
import numpy as np


def load_fakenews_data(file_path):
    """Load fakenews dataset
    
    :param file_path: path to data (csv format)
    :return: texts, labels
    :rtype: tuple
    """
    df = pd.read_csv(file_path)
    return list(df["cleaned_text"]), list(df["label"])


def label2index(_train_labels, _dev_labels, _test_labels):
    label2id = {}
    id2label = {}
    sorted_labels = sorted(np.unique(_train_labels).tolist())
    for i,lb in enumerate(sorted_labels):
        label2id[lb] = i
        id2label[i] = lb
    train_y = [label2id[lb] for lb in _train_labels]
    dev_y = [label2id[lb] for lb in _dev_labels]
    test_y = [label2id[lb] for lb in _test_labels]
    return train_y, dev_y, test_y, label2id, id2label
