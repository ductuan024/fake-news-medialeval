import numpy as np
import random
from sklearn import metrics
from tqdm.auto import tqdm, trange
import torch
from torch.utils.data import DataLoader, RandomSampler, SequentialSampler
from transformers import AdamW


def set_seed(seed=42):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)


def train(args, train_dataset, test_dataset, model):
    
    train_sampler = RandomSampler(train_dataset)
    train_dataloader = DataLoader(train_dataset, sampler=train_sampler,
                                  batch_size=args.train_batch_size)
    
    test_sampler = SequentialSampler(test_dataset)
    test_dataloader = DataLoader(test_dataset, sampler=test_sampler,
                                 batch_size=args.eval_batch_size)
    
    if args.optim == "AdamW":
        no_decay = ['bias', 'LayerNorm.weight']
        optimizer_grouped_parameters = [
            {'params': [p for n, p in model.named_parameters() if not any(nd in n for nd in no_decay)],
             'weight_decay': 0.01},
            {'params': [p for n, p in model.named_parameters() if any(nd in n for nd in no_decay)], 'weight_decay': 0.0}
        ]
        optimizer = AdamW(optimizer_grouped_parameters, lr=args.lr, eps=args.eps)
    else:
        optimizer = torch.optim.Adam(model.parameters(), lr=args.lr)
    
    print("***** Running training *****")
    print("  Num examples = %d" % len(train_dataset))
    print("  Num Epochs = %d" % args.epochs)
    
    set_seed(args.seed)
    for epoch in trange(args.epochs, desc="Epoch"):
        tr_loss = 0
        nb_tr_examples, nb_tr_steps = 0, 0

        epoch_iterator = tqdm(train_dataloader, desc="Iteration")
        for step, batch in enumerate(epoch_iterator):
            batch = tuple(t.to(args.device) for t in batch)
            inputs = {'input_ids': batch[0],
                      'attention_mask': batch[1],
                      'labels': batch[2]}
            model.train()
            
            outputs = model(**inputs)
            loss = outputs[0]  # model outputs are always tuple in transformers (see doc)
            if args.gradient_accumulation_steps > 1:
                loss = loss / args.gradient_accumulation_steps
                
            loss.backward()
            tr_loss += loss.item()
            
            if (step + 1) % args.gradient_accumulation_steps == 0:
                optimizer.step()
                model.zero_grad()
                
            nb_tr_examples += inputs['input_ids'].size(0)
            nb_tr_steps += 1
        
        print("Epoch {}, Train loss: {}".format(epoch+1, tr_loss/nb_tr_steps))
        
        model.eval()

        eval_loss = 0
        nb_eval_steps = 0
        predictions, true_labels = [], []
        
        eval_epoch_iterator = tqdm(test_dataloader, desc="Evaluation")
        for batch in eval_epoch_iterator:
            batch = tuple(t.to(args.device) for t in batch)
            with torch.no_grad():
                inputs = {
                    'input_ids': batch[0],
                    'attention_mask': batch[1],
                    'labels': batch[2]
                }
                b_labels = inputs['labels']
                outputs = model(**inputs)
                tmp_eval_loss, logits = outputs[:2]
            
            logits = logits.detach().cpu().numpy()
            label_ids = b_labels.to('cpu').numpy()
        
            predictions.append(logits)
            true_labels.append(label_ids)
            nb_eval_steps += 1
            eval_loss += tmp_eval_loss.item()
            
        flat_predictions = [item for sublist in predictions for item in sublist]
        flat_predictions = np.argmax(flat_predictions, axis=1).flatten()
        flat_true_labels = [item for sublist in true_labels for item in sublist]
        print(f"Epoch {epoch+1}, Validation loss: {eval_loss/nb_eval_steps}, "
              f"Validation Accuracy: {metrics.accuracy_score(flat_true_labels, flat_predictions)}, "
              f"Validation F1: {metrics.f1_score(flat_true_labels, flat_predictions, average='macro')}"
             )


def evaluate(args, model, test_dataset):
    test_sampler = SequentialSampler(test_dataset)
    test_dataloader = DataLoader(test_dataset, sampler=test_sampler,
                                 batch_size=args.eval_batch_size)
    model.eval()

    predictions, true_labels = [], []
    eval_epoch_iterator = tqdm(test_dataloader, desc="Evaluation")
    for batch in eval_epoch_iterator:
        batch = tuple(t.to(args.device) for t in batch)
        with torch.no_grad():
            inputs = {
                'input_ids': batch[0],
                'attention_mask': batch[1],
                'labels': batch[2]
            }
            b_labels = inputs['labels']
            outputs = model(**inputs)
            tmp_eval_loss, logits = outputs[:2]
    
        logits = logits.detach().cpu().numpy()
        label_ids = b_labels.to('cpu').numpy()
    
        predictions.append(logits)
        true_labels.append(label_ids)

    flat_predictions = [item for sublist in predictions for item in sublist]
    flat_predictions = np.argmax(flat_predictions, axis=1).flatten()
    flat_true_labels = [item for sublist in true_labels for item in sublist]
    print(f"Accuracy: {metrics.accuracy_score(flat_true_labels, flat_predictions)}, "
          f"F1: {metrics.f1_score(flat_true_labels, flat_predictions, average='macro')}"
          )
    





