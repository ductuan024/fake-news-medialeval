import torch
from torch.utils.data import TensorDataset
from keras.preprocessing.sequence import pad_sequences
from logzero import logger


def prepare_data(sentences, labels, tokenizer, maxlen=128):
    tokenized_texts = [tokenizer.tokenize(sent) for sent in sentences]

    # default truncated_method = "head"
    logger.info("Head-only Truncation")
    truncated_texts = [["CLS"] + text[:maxlen-2] + ["SEP"] for text in tokenized_texts]
    
    # Use the BERT tokenizer to convert the tokens to their index numbers in the BERT vocabulary
    input_ids = [tokenizer.convert_tokens_to_ids(x) for x in truncated_texts]
    
    # Pad our input tokens
    input_ids = pad_sequences(input_ids, maxlen=maxlen, dtype="long",
                              truncating="post", padding="post")
    
    # Create attention masks
    attention_masks = []
    
    # Create a mask of 1s for each token followed by 0s for padding
    for seq in input_ids:
        seq_mask = [float(i > 0) for i in seq]
        attention_masks.append(seq_mask)
    
    # Convert all of our data into torch tensors, the required datatype for our model
    inputs = torch.LongTensor(input_ids)
    labels = torch.LongTensor(labels)
    attention_masks = torch.LongTensor(attention_masks)
    
    dataset = TensorDataset(inputs, attention_masks, labels)
    
    return dataset

