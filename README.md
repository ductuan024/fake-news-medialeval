# FakeNews: Corona Virus and 5G Conspiracy Multimedia Twitter-Data-Based Analysis Task

Spontaneous and intentional digital Fake News wildfires over online social media can be as dangerous as natural fires. A new generation of data mining and analysis algorithms is required for early detection and tracking of such information cascades. This task focuses on the analysis of tweets related to Coronavirus and 5G conspiracy theories in order to detect misinformation spreaders.


## Announcements
* **19 August 2020:** The development set is now available to download.


## Task Schedule
* 20 August: Development set release
* 10 October: Test set release
* 23 October: Runs due
* 15 November: Results returned
* 30 November: Working notes paper
* Early December: MediaEval 2020 Workshop


## Task Description
The Fake News Detection Task offers two Fake News Detection subtasks on COVID-19 and 5G related conspiracy theories. The first subtask includes text-based Fake News detection while the second subtask targets the detection of abnormal spreading patterns. Both tasks are related to misinformation disseminated in the context of the COVID-19 crisis. Primarily, we are interested in conspiracy theories that assume a causal relationship between 5G and COVID-19, but tweets that spread other conspiracies theories connected to e.g., vaccination are labeled as well. 

***Text-Based Misinformation Detection***: In this subtask, the participants receive a dataset consisting of 10.000 tweet IDs in English related to COVID-19 and 5G, along with a script to download these tweets from Twitter. The participants are encouraged to build a binary or ternary classifier that can predict whether a tweet promotes conspiracy theories, either related to 5G or COVID-19 in general, or whether it only contains these buzzwords accidentally.

***Structure-Based Misinformation Detection***: Here, the participants receive a set of 
1902 graphs. Each graph is a subgraph of the Twitter follower graph and corresponds to a single tweet. The vertices are the accounts that posted or retweeted it. They are labeled by posting time. In addition, anonymized global IDs are available for training, but not for evaluation. The classification task is the same as above, i.e., distinguish the 5G or general conspiracy tweets from others. 


#### Motivation and Background
Digital wildfires, i.e., fast-spreading inaccurate, counterfactual, or intentionally misleading information, can quickly permeate public consciousness and have severe real-world implications, and they are among the top global risks in the 21st century. While a sheer endless amount of misinformation exists on the internet, only a small fraction of it spreads far and affects people to a degree where they commit harmful and/or criminal acts in the real world. The COVID-19 pandemic has severely affected people worldwide, and consequently, it has dominated world news for months. Thus, it is no surprise that it has also been the topic of a massive amount of misinformation, which was most likely amplified by the fact that many details about the virus were unknown at the start of the pandemic. We are interested in methods capable of detecting such misinformation. We consider primarily the narrative that *the COVID-19 outbreak is somehow connected to the introduction of the 5G wireless technology*, but also related conspiracy theories about COVID-19.  


#### Target Group
The task is of interest to researchers in the areas of online news, social media, multimedia analysis, multimedia information retrieval, natural language processing, and meaning understanding and situational awareness to participate in the challenge.


#### Data
The dataset contains two sets of tweets mentioning Corona Virus and 5G that include text, reposting time patterns, and the networks of reposters. The first set consists of only English language posts. It contains a wide variety of short, medium, and long tweets with neutral, positive, negative, and sarcastic phrasing. The second set consists of the networks and reposting time patterns. Both datasets are balanced with respect to the number of samples of conspiracy-promoting and other tweets, and the conspiracy class is further subdivided in 
5G related and other conspiracies.


## Task Steps

Participation in this task involves the following steps:
1. Download the development data and design your approach.
2. When the test set is released, run your system on the test data and submit up to 5 runs.
3. Receive your evaluation results (in terms of the official evaluation metric of the task), which you must report in the working notes paper.
4. Write and submit your working notes paper; different runs must all be described.


## Dataset

### Overview

The dataset consists of two main sets: annotated tweet IDs for ***Text-Based Misinformation Detection*** subtask and annotated retweet graphs for ***Structure-Based Misinformation Detection*** subtask that have been collected from Twitter during a period between 1st of January 2020 and 15th of July 2020, by searching for the 5G and Corona-virus-related keywords (e.g., "5G", "corona", "COVID-19", etc.) inside the tweets' text. All tweets should still be online at the time of releasing the dataset (we check for this regularly and will populate the update to the dataset). In order to be compliant with the Twitter Developer Policy, only the IDs of the tweets can be distributed, but an additional tool to download the complete tweets and embedded images are provided (our script is based on the one developed by Benjamin Bischke for the MediaEval 2018 event).

### Ground Truth

In both subtasks, we use three different class labels to mark the tweet contents: *5G-Corona Conspiracy*, *Other Consparacy* and *Non-Conspiracy*.

***5G-Corona Conspiracy*** This class contains all tweets that claim or insinuate some deeper connection between COVID-19 and 5G, such as the idea that 5G weakens the immune system and thus caused the current corona-virus pandemic, or that there is no pandemic and the COVID-19 victims were actually harmed by radiation emitted by 5G network towers. The crucial requirement is the claimed existence of some causal link.

***Other Consparacy*** This class contains all tweets that spread conspiracy theories other than the ones discussed above. This includes ideas about an intentional release of the virus, forced or harmful vaccinations, or the virus being a hoax.

***Non-Conspiracy*** This class contains all tweets not belonging to the previous two classes. Note that this also includes tweets that discuss COVID-19 pandemic itself, tweets claim that 5G is not proven to be absolutely safe or even can be harmful without linking it to COVID-19, as well as tweets claiming that authorities are pushing for the installation of 5G while the Publicis distracted by COVID-19. In addition, tweets pointing out the existence of conspiracy theories or mocking them fall into this class since they do not spread the conspiracy theories by inciting people to believe in them.


### Data release

#### Development Set

The following files are provided:

* `dev.zip` contains all the files of the development set and can be found [here](https://github.com/multimediaeval/2020-Fake-News-Detection-Task/blob/master/dataset/dev.zip)

***Text-Based Misinformation Detection*** subtask:

* `dev.zip/dev/tweets` zipped folder contains all the Tweet IDs split by the corresponding classes

* `dev.zip/dev/tweets/5g_corona_conspiracy.txt` zipped file contains all the Tweet IDs of the ***5G-Corona Conspiracy*** class in plain text format
* `dev.zip/dev/tweets/other_conspiracy.txt` zipped file contains all the Tweet IDs of the ***Other Consparacy*** class in plain text format
* `dev.zip/dev/tweets/non_conspiracy.txt` zipped file contains all the Tweet IDs of the ***Non-Conspiracy*** class in plain text format

* `dev.zip/dev/tweets/5g_corona_conspiracy.json` zipped file contains all the Tweet IDs of the ***5G-Corona Conspiracy*** class in JSON format
* `dev.zip/dev/tweets/other_conspiracy.json` zipped file contains all the Tweet IDs of the ***Other Consparacy*** class in JSON format
* `dev.zip/dev/tweets/non_conspiracy.json` zipped file contains all the Tweet IDs of the ***Non-Conspiracy*** class in JSON format

***Structure-Based Misinformation Detection*** subtask:

* `dev.zip/dev/graphs` zipped folder contains all the retweet graphs splitted by the correcponding classes

* `dev.zip/dev/graphs/5g_corona_conspiracy` zipped folder contains all the retweet graphs of the ***5G-Corona Conspiracy*** class
* `dev.zip/dev/graphs/other_conspiracy` zipped folder contains all the retweet graphs of the ***Other Consparacy*** class
* `dev.zip/dev/graphs/non_conspiracy` zipped folder contains all the retweet graphs of the ***Non-Conspiracy*** class

* `dev.zip/dev/graphs/<class_name>/<graph_sequential_number>` zipped folders contain the file sets describing the class-corresponding retweet graphs

* `dev.zip/dev/graphs/<class_name>/<graph_sequential_number>/nodes.csv` zipped files contain the definition of the follower nodes and their basic characteristics
* `dev.zip/dev/graphs/<class_name>/<graph_sequential_number>/edges.txt` zipped files contain the actual retweets between the defined nodes
* `dev.zip/dev/graphs/<class_name>/<graph_sequential_number>/plot.png` zipped files contain the reference visualization of the retweet graphs (provided for convinience, understanding and dataset reading verification)

#### Data format definition

*Tweet IDs* are stored in the plain text files contain one Tweet ID per line.

*Retweet Graphs* are stored individually, numbered in ascending order in a folder. Each graph-folder contains three files. 
* *edges.txt* file contains a directed edge list source-node-ID to target-node-ID. *plot.png* file containing a reference plot of the corresponding sub-graph. 
* *nodes.csv* contains an assignment from the node ID to the following properties: 
** `id` An anonymized node ID which remains the same for all graphs in the dataset of all categories
** `time` The time difference in seconds from each retweet to the original tweet. The original tweet always has a difference of 0 seconds to itself
** `friends` The next greater power of two of the follower count from the user profile of the respective user
** `followers` The next greater power of two of the friend count from the user profile of the respective user

***Please note that all the data contained in the *Retweet Graphs* subset is fully anonymized. Retweeting node IDs are replaced by the randomly generated numbers that, however, kept consistent along with all the graphs in the development set (and this consistency will *not* be kept in the test dataset). A Friend and A Follower counters are the exponent numbers that in the power of two exponentiation gives the number higher than the corresponding counter value. Thus, be aware, that it is *not possible* to link the provided retweet graphs to the real tweets or Twitter users.***

The number of nodes in the *nodes.csv* file does not necessarily match the number of nodes included in the *edges.txt* because nodes without any connections are not considered. We decided to provide this information anyway because it might be used as an additional input feature to the classifier.

The provided *Retweet Graphs* contain *subgraphs* of the Twitters Follower Graph. For each Tweet the Twitter API only provides a list of retweets, including the corresponding authors leading to star-shaped diffusion graphs. In other words, retweeting a retweet is considered the same as retweeting the original Tweet. Furthermore, a Retweet presents just a link pointing to the original Tweet, which also leads to comments appearing just underneath the original Tweet. Since each sub-graph must contain the trajectories of the real world spreading we recommend to pre-process the provided graphs and discard all edges that point against the time. 

Some of the graphs contain nodes that are not connected with any other component. There are several reasons for this phenomenon, all based on the fact that there is no actual follower relationship between any of the non-connected users and the remaining components of the subgraph. First, the user could have seen the Tweet/Retweet on an external source e.g., the New York Times website containing an embedded Tweet object. Second, the Tweet/Retweet could have been retweeted by manually visiting a twitter user profile without following it.


### Data downloading for ***Text-Based Misinformation Detection*** subtask

The script found [here](https://github.com/multimediaeval/2020-Fake-News-Detection-Task/blob/master/tools/twitter_downloader). It is developed by Benjamin Bischke and provides you support for downloading tweets and embedded images using the distributed files of Tweet IDs, since we are not allowed, unfortunately, to distribute the original tweets and images. Please follow the instructions below. If you have any questions or problems, please contact the task organizers.

#### Setup

##### 1. Environmental Setup:
* Install Python on your system (https://www.python.org/downloads/) if you do not already use it. The script works for both versions - Python 2.7 and Python 3.
* Make sure all required python packages are installed by executing: `pip install -r requirements.txt` in your console. The `requirements.txt` can be found [here](https://github.com/multimediaeval/2020-Flood-Related-Multimedia-Task/tree/master/tools/twitter_downloader)
* If you are using Python 3.7, then install Tweepy library as follows: `pip3 install git+https://github.com/tweepy/tweepy.git`

##### 2. Twitter-API Setup:
* Create a Twitter Account if you do not already have one. (https://twitter.com/i/flow/signup)
* Follow this guide to retrieve access credentials for the Twitter API. (https://developer.twitter.com/en/docs/basics/authentication/oauth-1-0a/obtaining-user-access-tokens)
* Set your private keys in the beginning of the script `download_tweets.py` (lines 65-68)

#### Usage
You can start the download with the following call:
```
python download_tweets.py -f ./path/to/the/file/devset_tweets_ids.json -o /your/output/directory
```

With the first option, you can specify the path to the distributed Tweet IDs file and with the second option the path to the output folder, in which the Twitter data should be stored.

If you have more than 4 cores available on your system and you want to speed up the download of the images, you can parallelize among more processes by specifying the option `-p YourNumberOfProcesses`.

For more options, please see `python download_tweets.py -h`.


## Submission

### Run Submissions

For this task, you may submit up to 5 runs for each of the two subtasks. Please be sure to submit the following types of runs:

***Text-Based Misinformation Detection*** subtask:
1. **Required run:** automated tweet classification based only on tweet text analysis
2. Optional run: automated tweet classification based on tweet text analysis in combination with visual information extracted from the original tweet
3-5. Optional runs: relaxed automated tweet classification using any automatically scraped data from any external sources

***Structure-Based Misinformation Detection*** subtask:
1. **Required run:** automated tweet classification based only on provided retweet graph structure (using only *edges.txt* files)
2. Optional run: automated tweet classification based on the full set of retweet graph description (using *edges.txt*, *nodes.csv* and *plot.png* files)
3-5. Optional runs: relaxed automated retweet graph classification using any automatically scraped data from any external sources

Please note that for generating runs 1 to 2 in both subtasks participants are allowed to use only information that can be extracted from the provided tweets (including metadata) and retweet graphs, while for runs 3 to 5 everything is allowed, both from the method point of view and the information sources. However, manual annotation of tweets or any external data is not allowed in any run.

### Submission Format

***Text-Based Misinformation Detection*** subtask:
Please submit your runs for the task in the form of a text file, where each line contains one Tweet ID followed by the label for the detected class:
* 0 = cannot determine
* 1 = 5G-Corona conspiracy
* 2 = Other Conspiracy
* 3 = Non-Conspiracy
Tweet IDs and the labels should be comma-separated. An example should look like this:

```
1249230759589810176,1
1249076746533720068,2
...
1226581653193187329,3
```

***Structure-Based Misinformation Detection*** subtask:
Please submit your runs for the task in the form of a text file, where each line contains one sequential graph number (matches the graph folder name) followed by the label for the detected class:
* 0 = cannot determine
* 1 = 5G-Corona conspiracy
* 2 = Other Conspiracy
* 3 = Non-Conspiracy
Sequential graph number (matches the graph folder name) and the labels should be comma-separated. An example should look like this:

```
1,3
2,1
...
150,3
```


Please note that no white spaces or other special characters are allowed. When you create the filenames for your runs, please follow this pattern:

```
ME20FND_YYY_ZZZ.txt
```

where ME20FND is the code of the task, YYY is the acronym of your team and ZZZ is the number of your run. For the ***Text-Based Misinformation Detection*** subtask run numbers are 001, 002, 003, 004 and 005. For the ***Structure-Based Misinformation Detection*** subtask run numbers are 101, 102, 103, 104 and 105.

Instructions about where to upload the resulting runs will follow.


#### Evaluation Methodology
The ground truth is created by manual annotation of the collected tweets based on text-only analysis by a group of experts and volunteers. The main metric will be ROC AUC, but the task may explore other metrics as well.


#### References and recommended reading

***General***

[1] Nyhan, Brendan, and Jason Reifler. 2015. [Displacing misinformation about events: An experimental test of causal corrections](https://www.cambridge.org/core/journals/journal-of-experimental-political-science/article/displacing-misinformation-about-events-an-experimental-test-of-causal-corrections/69550AB61F4E3F7C2CD03532FC740D05#). Journal of Experimental Political Science 2, no. 1, 81-93.

***Twitter data collection and analysis***

[2] Schroeder, Daniel Thilo, Konstantin Pogorelov, and Johannes Langguth. 2019. [FACT: a Framework for Analysis and Capture of Twitter Graphs](https://ieeexplore.ieee.org/document/8931870). In 2019 Sixth International Conference on Social Networks Analysis, Management and Security (SNAMS), IEEE, 134-141.

[3] Achrekar, Harshavardhan, Avinash Gandhe, Ross Lazarus, Ssu-Hsin Yu, and Benyuan Liu. 2011. [Predicting flu trends using twitter data](https://ieeexplore.ieee.org/document/5928903). In 2011 IEEE conference on computer communications workshops (INFOCOM WKSHPS), IEEE, 702-707.

[4] Chen, Emily, Kristina Lerman, and Emilio Ferrara. 2020. [Covid-19: The first public coronavirus twitter dataset](https://arxiv.org/abs/2003.07372v1?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+CoronavirusArXiv+%28Coronavirus+Research+at+ArXiv%29). arXiv preprint arXiv:2003.07372.

[5] Kouzy, Ramez, Joseph Abi Jaoude, Afif Kraitem, Molly B. El Alam, Basil Karam, Elio Adib, Jabra Zarka, Cindy Traboulsi, Elie W. Akl, and Khalil Baddour. 2020. [Coronavirus goes viral: quantifying the COVID-19 misinformation epidemic on Twitter](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7152572/). Cureus 12, no. 3.

***Natural language processing***

[6] Bourgonje, Peter, Julian Moreno Schneider, and Georg Rehm. 2017. [From clickbait to fake news detection: an approach based on detecting the stance of headlines to articles](https://www.aclweb.org/anthology/W17-4215/). In Proceedings of the 2017 EMNLP Workshop: Natural Language Processing meets Journalism, 84-89.

[7] Imran, Muhammad, Prasenjit Mitra, and Carlos Castillo. 2016. [Twitter as a lifeline: Human-annotated twitter corpora for NLP of crisis-related messages](https://arxiv.org/abs/1605.05894). arXiv preprint arXiv:1605.05894.

***Information spreading***

[8] Liu, Chuang, Xiu-Xiu Zhan, Zi-Ke Zhang, Gui-Quan Sun, and Pak Ming Hui. 2015. [How events determine spreading patterns: information transmission via internal and external influences on social networks](https://iopscience.iop.org/article/10.1088/1367-2630/17/11/113045/pdf). New Journal of Physics 17, no. 11.


#### Task Organizers
* Konstantin Pogorelov, Simula Research laboratory (Simula), Norway, konstantin (at) simula.no
* Johannes Langguth, Simula Research laboratory (Simula), Norway, langguth (at) simula.no
* Daniel Thilo Schroeder, Simula Research laboratory (Simula), Norway


## Acknowledgements

[UMOD project](https://www.simula.no/research/projects/umod-understanding-and-monitoring-digital-wildfires) is funded by the Norwegian SAMRISK-2 project ''UMOD'' (\#272019) and has benefited from the Experimental Infrastructure for Exploration of Exascale Computing (eX3), which is financially supported by the Research Council of Norway under contract 270053.
